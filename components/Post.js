import { Container } from 'reactstrap';
import HeadMeta from './HeadMeta';
import { decodeEntities, setDefaultMeta } from '../lib/utils';

const Post = props => {
  const { content } = props;
  const { yoast, link, _embedded } = content;

  const meta = setDefaultMeta(yoast, content);

  const featuredImage = media => (
    <picture>
      <source media="(max-width: 768px)" srcSet={media.media_details.sizes['large']} />
      <img src={media.media_details.sizes['full'].source_url} alt={media.alt_text} className="img-fluid" />
    </picture>
  );

  return  (
    <Container>
      <HeadMeta meta={meta} link={link} />
      
      <h1 className="font-conduit-bold my-4">{decodeEntities(content.title.rendered)}</h1>

      {_embedded['wp:featuredmedia'][0] && featuredImage(_embedded['wp:featuredmedia'][0])}
      
      <div dangerouslySetInnerHTML={{ __html: content.content.rendered }} className="my-5"></div>
    </Container>
  );
};

export default Post;