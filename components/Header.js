import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import { Link } from '../routes';
import {
  Container,
  Navbar,
  NavbarBrand,
  Collapse,
  Nav,
  NavItem,
  NavLink,
  Button
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';

const Header = props => {
  const [ toggle, setToggle ] = useState(false);
  const [ sticky, setSticky ] = useState(false);
  const [ loaded, setLoaded ] = useState(false);

  const linkClasses = 'text-uppercase font-weight-bold text-center text-white font-conduit-bold';

  return (
    <header className={`${!loaded ? 'd-none' : ''} fixed-top py-md-4 ${sticky ? 'bg-primary sticky' : ''}`}>
      <Collapse isOpen={toggle} navbar className="position-absolute w-100 bg-primary" style={{top:0}}>
        <Nav navbar>
          <NavItem>
            <Link route="/about" passHref>
              <NavLink className={linkClasses}>About</NavLink>
            </Link>
          </NavItem>
        </Nav>
      </Collapse>
      <Navbar expand={null} className="px-0">
        <Container>
          <Link route="/" passHref>
            <NavbarBrand><img src={`/static/${(toggle || sticky) ? 'logo-white.png' : 'logo.png'}`} alt="Fallon Logo" /></NavbarBrand>
          </Link>
          <Button color="link" className={`p-0 ${(toggle || sticky) && 'text-white'}`} onClick={() => setToggle(!toggle)}>{toggle ? <FontAwesomeIcon icon={faTimes} size="2x" /> : <FontAwesomeIcon icon={faBars} size="2x" />}</Button>
        </Container>
      </Navbar>
    </header>
  );
};

const mapStateToProps = state => state;
// const mapDispatchToProps = dispatch({ setWorkHits });
export default connect(mapStateToProps)(withRouter(Header));