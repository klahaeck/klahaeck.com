import { Container } from 'reactstrap';
import HeadMeta from './HeadMeta';
import { decodeEntities, setDefaultMeta } from '../lib/utils';

const Page = props => {
  const { content } = props;
  const { yoast, link, title, _embedded } = content;

  const meta = setDefaultMeta(yoast, content);

  return (
    <Container>
      <HeadMeta meta={meta} link={link} />

      <h1 className="font-conduit-bold my-4">{decodeEntities(title.rendered)}</h1>

      {_embedded['wp:featuredmedia'] && _embedded['wp:featuredmedia'][0].media_details.sizes['power2-lg'].source_url !== '' && <img src={_embedded['wp:featuredmedia'][0].media_details.sizes['power2-lg'].source_url} alt={_embedded['wp:featuredmedia'][0].alt_text} className="img-fluid" />}

      <div dangerouslySetInnerHTML={{ __html: content.content.rendered }}></div>
    </Container>
  );
};

export default Page;