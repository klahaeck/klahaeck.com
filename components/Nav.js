import { Link } from '../routes';

const Nav = props => {
  return (
    <ul>
      <li>
        <Link route="/" passHref>
          <a>Home</a>
        </Link>
      </li>
      <li>
        <Link route="/about" passHref>
          <a>About</a>
        </Link>
      </li>
    </ul>
  );
};

export default Nav;