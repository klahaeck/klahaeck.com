import { Link } from '../routes';
import { Container, Row, Col } from 'reactstrap';

const Footer = () => {
  return (
    <footer className="mt-5">
      <Container className="px-0 px-sm-3">
        Footer
      </Container>
    </footer>
  );
};

export default Footer;