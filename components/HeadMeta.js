import Head from 'next/head';
import getConfig from 'next/config';
import defaultsDeep from 'lodash/defaultsDeep';

const HeadMeta = props => {
  const { publicRuntimeConfig } = getConfig();
  const { meta, link } = props;

  const defaultMeta = {
    'opengraph-image': meta['opengraph-image'] || publicRuntimeConfig.defaultShareImageUrl,
    'twitter-image': meta['twitter-image'] || publicRuntimeConfig.defaultShareImageUrl
  };

  const mergedMeta = defaultsDeep(defaultMeta, meta);

  return (
    <Head>
      {mergedMeta.title && <title key="title">{mergedMeta.title}</title>}
      {mergedMeta.metadesc !== '' && <meta name="description" content={mergedMeta.metadesc} key="description" />}
      {mergedMeta.metakeywords !== '' && <meta name="keywords" content={mergedMeta.metakeywords} key="keywords" />}

      {mergedMeta['opengraph-title'] !== '' && <meta property="og:title" content={mergedMeta['opengraph-title']} key="opengraph-title" />}
      {link !== '' && <meta property="og:url" content={link} key="opengraph-url" />}
      {mergedMeta['opengraph-image'] !== '' && <meta property="og:image" content={mergedMeta['opengraph-image']} key="opengraph-image" />}

      {mergedMeta['opengraph-image'] !== '' && <meta property="og:image:secure_url" content={mergedMeta['opengraph-image-secureurl']} key="opengraph-image-secureurl" />}
      
      {mergedMeta['opengraph-description'] !== '' && <meta property="og:description" content={mergedMeta['opengraph-description']} key="opengraph-description" />}
      {mergedMeta['twitter-title'] !== '' && <meta name="twitter:title" content={mergedMeta['twitter-title']} key="twitter-title" />}
      {mergedMeta['twitter-description'] !== '' && <meta name="twitter:description" content={mergedMeta['twitter-description']} key="twitter-description" />}
      {mergedMeta['twitter-image'] && <meta name="twitter:image" content={mergedMeta['twitter-image']} key="twitter-image" />}
    </Head>
  );
};

export default HeadMeta;