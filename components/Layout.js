import Head from 'next/head';
import Header from './Header';
import Footer from './Footer';
// import Loader from './Loader';
// import { PageTransition } from 'next-page-transitions';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faBars } from '@fortawesome/free-solid-svg-icons';

const Layout = props => {
  return (
    <>
      <Head>
        <title key="title">Kla Haeck</title>
        <meta charSet='utf-8' />
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />

        <meta name="description" content="" key="description" />
        <meta name="keywords" content="" key="keywords" />

        <meta property="og:site_name" content="Kla Haeck" />
        <meta property="og:title" content="Home - Kla Haeck" key="opengraph-title" />
        <meta property="og:url" content="https://www.klahaeck.com/" key="opengraph-url" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="" key="opengraph-image" />
        <meta property="og:image:secure_url" content="" key="opengraph-image-secureurl" />
        <meta property="og:image:width" content="1200" key="opengraph-image-width" />
        <meta property="og:image:height" content="630" key="opengraph-image-height" />
        {/* <meta property="fb:app_id" content="161775270504978" /> */}
        <meta property="og:description" content="" key="opengraph-description" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@klahaeck" />
        <meta name="twitter:title" content="Home - Kla Haeck" key="twitter-title" />
        <meta name="twitter:description" content="" key="twitter-description" />
        <meta name="twitter:image" content="" key="twitter-image" />
        {/* <script dangerouslySetInnerHTML={{
          __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-585D2QF');`,
        }}>
        </script> */}
      </Head>
      <Header />
      {/* <PageTransition
        timeout={300}
        classNames="page-transition"
        loadingComponent={<Loader />}
        loadingDelay={0}
        loadingTimeout={0}
        // loadingTimeout={{
        //   enter: 400,
        //   exit: 0,
        // }}
        // loadingClassNames="loading-indicator"
      > */}
        {props.children}
      {/* </PageTransition> */}
      {/* <style jsx global>{`
        .page-transition-enter {
          opacity: 0;
        }
        .page-transition-enter-active {
          opacity: 1;
          transition: opacity 300ms;
        }
        .page-transition-exit {
          opacity: 1;
        }
        .page-transition-exit-active {
          opacity: 0;
          transition: opacity 300ms;
        }
      `}</style> */}
      <Footer />
    </>
  );
};

export default Layout;
