const { withPlugins, optional } = require('next-compose-plugins');
// const webpack = require('webpack');
// const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
// const { PHASE_DEVELOPMENT_SERVER } = require('next-server/constants');
const { parsed: localEnv } = require('dotenv').config();

const imagesConfig = {};
// const cssConfig = {};
const sassConfig = {
  // cssModules: true
};

// console.log(localEnv);

const nextConfig = {
  useFileSystemPublicRoutes: false,
  serverRuntimeConfig: { // Will only be available on the server side
    
  },
  publicRuntimeConfig: { // Will be available on both server and client
    apiURL: localEnv.API_URL
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf|afm|otf|svg|png|jpg|gif)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          name: '[name].[ext]'
        }
      }
    })
    return config
  }
};

module.exports = withPlugins([
  // [withCSS, cssConfig],
  [withSass, sassConfig],
  [withImages, imagesConfig],
  // [optional(() => require('@zeit/next-sass')), sassConfig, [PHASE_DEVELOPMENT_SERVER]],
], nextConfig);