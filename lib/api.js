import getConfig from 'next/config';
import WPAPI from 'wpapi';
const { publicRuntimeConfig } = getConfig();

let endpoint = publicRuntimeConfig.apiURL;
const api = new WPAPI({ endpoint });
// api.crew = api.registerRoute('wp/v2', '/crew/(?P<id>\\d+)', {
//   params: [ 'department' ]
// });
// api.jobs = api.registerRoute('wp/v2', '/jobs/(?P<id>\\d+)');

export default api;
