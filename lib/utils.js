import defaultsDeep from 'lodash/defaultsDeep';

export const decodeEntities = encodedString => {
  const translate_re = /&(nbsp|amp|quot|lt|gt|reg);/g;
  const translate = {
    "nbsp":" ",
    "amp" : "&",
    "quot": "\"",
    "lt"  : "<",
    "gt"  : ">",
    "reg" : "®",
  };
  return encodedString.replace(translate_re, function(match, entity) {
    return translate[entity];
  }).replace(/&#(\d+);/gi, function(match, numStr) {
    const num = parseInt(numStr, 10);
    return String.fromCharCode(num);
  });
};

// export const youtubeParser = url => {
//   var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
//   var match = url.match(regExp);
//   return (match && match[7].length == 11) ? match[7] : false;
// };

export const randomIntFromInterval = (min, max) => { // min and max included
  return Math.floor(Math.random()*(max-min+1)+min);
};

export const setDefaultMeta = (yoast, content) => {
  const featuredImage = content._embedded && content._embedded['wp:featuredmedia'] ? content._embedded['wp:featuredmedia'][0].media_details.sizes['large'].source_url : '';
  const defaultMeta = {
    title: yoast.title || `${content.title.rendered} - Fallon`,
    'opengraph-title': yoast['opengraph-title'] || `${content.title.rendered} - Fallon`,
    'twitter-title': yoast['twitter-title'] || `${content.title.rendered} - Fallon`,
    'opengraph-image': yoast['opengraph-image'] || featuredImage,
    'twitter-image': yoast['twitter-image'] || featuredImage
  };

  const meta = defaultsDeep(defaultMeta, yoast);
  return meta;
};