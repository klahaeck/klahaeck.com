const routes = require('next-routes');

module.exports = routes()
  .add('index', '/')
  .add('post', '/blog/:slug')
  .add('page', '/:slug');
  // .add('product-size', '/our-menu/:category/:product/:size', 'product')
  // .add('product', '/our-menu/:category/:product')
  // .add('our-menu', '/our-menu/:category');
