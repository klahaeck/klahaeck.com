const express = require('express');
const next = require('next');
const routes = require('./routes');
// const cors = require('cors');
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const server = express();
const LRUCache = require('lru-cache');
const port = process.env.PORT || 3000;

const ssrCache = new LRUCache({
  max: 100,
  maxAge: 1000 * 60 * 60 // 1hour
});

app.prepare().then(() => {
  // server.use(cors());
  // server.options('*', cors());
  server.use(renderAndCache).listen(port);
}).catch(ex => {
  console.error(ex.stack);
  process.exit(1);
});

function renderAndCache (req, res) {
  if (!dev && ssrCache.has(req.url)) {
    return res.send(ssrCache.get(req.url))
  }

  // Match route + parse params
  const {route, params} = routes.match(req.url)
  if (!route) return handle(req, res)

  app.renderToHTML(req, res, route.page, params).then((html) => {
    ssrCache.set(req.url, html)
    res.send(html)
  })
  .catch((err) => {
    app.renderError(err, req, res, route.page, params)
  })
}
