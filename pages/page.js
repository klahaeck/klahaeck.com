import Layout from '../components/Layout';
import api from '../lib/api';
import Page from '../components/Page';

const PagePage = props => {
  return (
    <Layout>
      <Page {...props} />
    </Layout>
  );
};

PagePage.getInitialProps = async function(props) {
  const { query: { slug } } = props;
  const pageContent = await api.pages().slug(slug).embed();
  return { content: pageContent[0] };
};

// const mapStateToProps = (state) => (state);
// const mapDispatchToProps = { setWorkRecent };
// export default connect(state => state)(PostPage);
export default PagePage;