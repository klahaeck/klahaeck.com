// import { connect } from 'react-redux';
// import {} from '../store';
import Layout from '../components/Layout';
import api from '../lib/api';
import Post from '../components/Post';

const PostPage = props => {
  return (
    <Layout>
      <Post {...props} />
    </Layout>
  );
};

PostPage.getInitialProps = async function(props) {
  const { query: { slug } } = props;
  const postContent = await api.posts().slug(slug).embed();
  return { content: postContent[0] };
};

// const mapStateToProps = (state) => (state);
// const mapDispatchToProps = { setWorkRecent };
// export default connect(state => state)(PostPage);
export default PostPage;
