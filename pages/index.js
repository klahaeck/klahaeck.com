import Layout from '../components/Layout';
import api from '../lib/api';
import Home from '../components/Home';

const Index = props => {
  return (
    <Layout>
      <Home {...props} />
    </Layout>
  );
};

Index.getInitialProps = async props => {
  // const { req } = props;
  // const pageContent = await api.pages().slug('home').embed();
  // return { content: pageContent[0] };
  return { content: 'testing' }
};

export default Index;