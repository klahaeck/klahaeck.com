// _document is only rendered on the server side and not on the client side
// Event handlers like onClick can't be added to this file

import Document, { Head, Main, NextScript } from 'next/document';
import flush from 'styled-jsx/server';
import useragent from 'useragent';

class MyDocument extends Document {
  static getInitialProps(props) {
    // console.log(req);
    const { req, res, renderPage, isServer = false } = props;
    const { html, head, errorHtml, chunks } = renderPage();
    const styles = flush();
    const ua = useragent.parse(req.headers['user-agent']);
    return { html, head, errorHtml, chunks, styles, useragent: ua };
  }
  
  render() {
    const { styles, useragent } = this.props;

    return (
      <html lang="en">
        <Head>
          <style>{styles}</style>
          {
            useragent.family === 'IE' && ( // IE only, not Edge or others
              <script
                src='https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.23.0/polyfill.min.js'
              />
            )
          }
        </Head>
        <body>
          {/* <noscript dangerouslySetInnerHTML={{
            __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-585D2QF" height="0" width="0" style="display:none;visibility:hidden"></iframe>`,
          }}>
          </noscript> */}
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}

export default MyDocument;
