import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

export const initialState = {
  // currentSection: '',
  // cases: [],
};

export const actionTypes = {
  // NAVIGATE_SECTION: 'NAVIGATE_SECTION',
  // SET_CASES: 'SET_CASES'
};

// REDUCERS
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    // case actionTypes.NAVIGATE_SECTION:
    //   return Object.assign({}, state, {
    //     currentSection: action.currentSection
    //   });
    // case actionTypes.SET_CASES:
    //   return Object.assign({}, state, {
    //     cases: action.cases
    //   });
    default:
      return state;
  }
};

// ACTIONS
// export const setCurrentSection = currentSection => {
//   return { type: actionTypes.NAVIGATE_SECTION, currentSection }
// };
// export const setCases = cases => {
//   return { type: actionTypes.SET_CASES, cases }
// };

export const initializeStore = (_initialState = initialState, {}) => {
  return createStore(reducer, _initialState, composeWithDevTools(applyMiddleware()));
};